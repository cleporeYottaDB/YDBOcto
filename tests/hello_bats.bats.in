#################################################################
#								#
# Copyright (c) 2019 YottaDB LLC and/or its subsidiaries.	#
# All rights reserved.						#
#								#
#	This source code contains the intellectual property	#
#	of its copyright holder(s), and is made available	#
#	under a license.  If you do not know the terms of	#
#	the license, please stop and do not read further.	#
#								#
#################################################################

load test_helpers

setup() {
  init_test
}

@test "make sure the test system works" {
  run echo "Hello world!"
}

@test "ensure that we can create a database for testing" {
  createdb
  run ls mumps.dat
  copy_test_files helloBats/hello1.sql
  copy_test_files helloBats/hello2.sql
  $ydb_dist/mumps -r %XCMD "set ^abc(0)=0"
  run octo -f helloBats/hello1.sql
  run octo -f helloBats/hello2.sql
  echo $output &> output.txt
  [[ "$output" =~ "Hello world" ]]
}

@test "can load basic fixtures" {
  createdb
  load_fixture names.sql
  load_fixture names.zwr
  octo -vvv >& output.txt <<OCTO
select * from names;
OCTO
  run cat output.txt
  [[ "$output" =~ "Lord|Nikon" ]]
}

@test "can communicate with PostgreSQL server" {
  create_postgres_database names
  load_postgres_fixture names postgres-names.sql
  create_postgres_database customers
  load_postgres_fixture customers postgres-customers.sql
  create_postgres_database pastas
  load_postgres_fixture pastas postgres-pastas.sql
  create_postgres_database easynames
  load_postgres_fixture easynames postgres-easynames.sql

  # Check names database
  psql names >& output.txt <<PSQL
select * from names;
PSQL
  run cat output.txt
  [[ "$output" =~ "  5 | Zero                           | Cool " ]]

  # Check customers database
  psql customers >& output.txt <<PSQL
select * from customers;
PSQL
  run cat output.txt
  [[ "$output" =~ "           5 | James      | Monroe" ]]
}
